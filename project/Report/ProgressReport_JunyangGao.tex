\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
% \PassOptionsToPackage{numbers, compress}{natbib}
% before loading nips_2018

% ready for submission
% \usepackage{nips_2018}

% to compile a preprint version, e.g., for submission to arXiv, add
% add the [preprint] option:
\usepackage[preprint]{nips_2018}

% to compile a camera-ready version, add the [final] option, e.g.:
% \usepackage[final]{nips_2018}

% to avoid loading the natbib package, add option nonatbib:
% \usepackage[nonatbib]{nips_2018}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{graphicx}
\usepackage{bm}
\usepackage{amsthm}
\usepackage{caption}
 
\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]


\title{Applying Sequence Model to Basketball Play-by-Play Data Analysis}

% The \author macro works with any number of authors. There are two
% commands used to separate the names and addresses of multiple
% authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to
% break the lines. Using \AND forces a line break at that point. So,
% if LaTeX puts 3 of 4 authors names on the first line, and the last
% on the second line, try using \AND instead of \And before the third
% author name.

\author{
  Junyang Gao \\
  \texttt{jygao@cs.duke.edu} \\
  %% examples of more authors
  %% \And
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
  %% \AND
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
  %% \And
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
  %% \And
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
}

\begin{document}
% \nipsfinalcopy is no longer used

\maketitle

\begin{abstract}
  In this project, we explore the play-by-play shotlog data in National Collegiate Athletic Association (NCAA) baseketball games by applying recurrent neural network (RNN). 
  Play-by-play data reveals the \emph{temporal} dependence of each event of the game, and RNN could learn and model the temporal dynamics from structured sequence data. 
  Our hope is to combine deep sequence model and play-by-play data to provide more insights into traditional sports data analytics.
  More specifically, we focus on two novel and interesting tasks: (1) Team Segmentation and (2) Synthetic Play Generation.
  The first task on team segmentation is to find groups of similar NCAA teams. We use a RNN to generate encodings (results of intermediate layers) for sequence play-by-play data of each team, and then apply clustering algorithm in the encoding space to identify groups. 
  We argue that RNN and encodings of a neural network provides be an effective and novel way for clsutering teams. 
  The segmentation results could provide team coaches, players and basketball fans a better understanding of the NCAA games and teams.
  The second task on synthetic play generation is a by-product of RNN. Given a trained RNN, it can be used as a generative model by using its internal hidden states. Though it's generally hard to evaluate the synthetic play outcomes, the task still provides interesting and (possibly) insightful results for both professionals and fans.



  
\end{abstract}

\section{Introduction}
\label{sec:intro}
There are emerging trends for sports data analytics of using player and ball tracking data — Since 2013, NBA has applied an advanced tracking system to produce real-time player and ball positional data for every single game; In 2015, Duke led the College Basketball’s data revolution, being the first NCAA team to embrace the cutting-edge tracking system for generating play-by-play motion and tracking data. This sea of information provides us with unlimited probabilities for more insights of the game.
Despite the fact that we have easy access to the large amount of high quality tracking data, how to effectively analyze this new form of the data has been largely ignored by prior research. In this project, we aim to go beyond the traditional box score or statistics, combining deep learning techniques (specifically, Sequence Models) and detailed play-by-play data logs to produce novel analytics, including clustering and synthetic play generation, that traditional data mining algorithms or machine learning models can’t handle.

\section{Data Description}
\label{sec:data}
There are many types of play-by-play data, i.e., ball movement and player movement. 
In this project, we focus on the team's shot logs, which mainly records the shooting location (XY\--coordinates), shooting time\--stamp and other related labels of the shot, including shot types (three\-pointer, jump shot, layup), outcomes (miss or made), etc.
Given a team and a game, we use a sequence of tuples organized by time $X_i = \{(t_1,x_1,y_1, l_1),(t_2,x_2,y_2, l_2), \cdots, (t_n, x_n, y_n, l_n)\}$ to represent its shots logs, where $t$ is the shooting time\--stamp in seconds, $xy$ is the shooting location in the basketball court and $l$ is the label associated with the shot. More specifically, $x, y$ represent the location of the shot in number of inches from the "left" baseline (max: 1128) and from the ``top'' sideline (max: 600), respectively. 

\subsection{Data Source}
\emph{Google BigQuery} hosts a historical play-by-play shotlog dataset \footnote{\url{https://bigquery.cloud.google.com/table/bigquery-public-data:ncaa_basketball.mbb_pbp_sr?pli=1}} containing NCAA tournament games from 2013 to 2017. 
We queried the entire dataset and downloaded the query result as our data. It can be find at our gitlab repository. \footnote{\url{https://gitlab.oit.duke.edu/jg308/IntroDeepLearning/blob/master/project/ncaa-shotlog.csv}}.  
The dataset contains 19,494 shotlogs from 231 distinct NCAA teams, where each shotlog is a shot sequence with variable length (i.e, how many shots made by the team in a game) as $X_i$ ordered by time. 

\subsection{Data Preprocessing}
\paragraph{Team Pruning}
Games are not evenly distributed over 231 teams - the max number of games from a team is 478, while the min number is just 1. 
We need enough games to sufficiently learn and model the style of each team. Hence, we prune the teams with games recorded less than 100. 
After the prunning, we have 15,464 game shotlogs from 85 distinct NCAA teams, averaging 182 games for each team.

\paragraph{Shot Position Transformation}
\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{shotlogs_visual}
  \caption{Shotlog Visualization\label{fig:shotlog_visual}}
\end{figure}
A NCAA game consists of two 20-minutes halves, and teams switch baskets to goal at halftime. If we look at the raw shotlog data with their absolute shooting coordinates, there is clearly a position shift from one section of the court to another, as shown in fig~\ref{fig:shotlog_visual}(a). In order to get rid of the suddent changes in shooting coordinates, we limit the shooting position to the left side of the court, and apply the following the transformation of positions on the right side of the court:
\begin{equation}
(x',y') = (1128 - x, 600 - y),
\end{equation} 
shown as fig~\ref{fig:shotlog_visual}(b).

\section{Problem Statement}
In this project, we focus on tackling the following two novel and interesting tasks in sports data analytics: (1) Team Segmentation, and (2) Synthetic Play Generation.

\paragraph{Team Segmentation}
Analytically, team segmentations involve clustering all NCAA teams to find groups of similar teams. 
Understanding team segments could provide deeper insights into the game - team coaches might design suitable strategies to teams from different segments (instead of for each individual game!) for the win, and basketball fans may find it an interesting topic to discuss.
However, it's not clear how to define ``similar'' in the context of basketball team - by demographic? by win/loss ratio? 
There is not a gold stardard for similarity. On the other hand, traditionally, the data that goes into the clustering is often limited by the clsutering algorithms themselves - most require some kind of tabular data structure, e.g., distance matrix, and common techniques like $k$-Means require strictly numeric input.

Intuitively, teams should be segmented or clustered by the games they played. With play-by-play shotlog data in hand, we are capable of understanding the style or tendencies of shots made by a team, and then group team based on these features.
Given the popularity of deep learning models and techniques, i.e., RNN, to tackle sequence-related learning tasks, it's natural to use a RNN to learn the hidden features (encodings) that represents a team.
\paragraph{Synthetic Play Generation}
After learning the shotlog sequence data for each team using RNN, the internal hidden states encode the temporal dependences among sequence data. Given an initial input, the network can also be used as a generative model to automatically generate synthetic shots that follows the similar trends of the training data. 
Although we realize that it's empirically hard to evaluate shot generations, the synthetic generative outcomes still provides interesting and (possibly) insightful results for both professionals and fans.

\section{Core Methods}
\begin{figure}
\centering
\begin{minipage}{0.45\textwidth}
\includegraphics[width=\linewidth]{RNN_Encoding.png}
\caption{Network Architecture \label{fig:rnn}}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\captionsetup{type=table} %% tell latex to change to table
\caption{Network Summary}
  \label{tbl:summary}
  \centering
  \small
  \begin{tabular}{lll}
    \toprule
    Layer& Input Dimension & Output Dimension \\
    \toprule
    RNN & Variable & N (tunning hyperparameters) \\
    Encoding & N & N (used for encoding) \\
    Output & N & n (distinct number of teams) \\
    \bottomrule
  \end{tabular}
\end{minipage}
\end{figure}
In this section, we present our approach to solve the Team Segmentation problem.
\paragraph{Sequence Modeling with RNN}
An RNN can take as input a variable-length sequence $x = (x_1, x_2, \dots, x_T)$ by recursively processing each symbol while maintaining its internal hidden state $\bm{h}$. At each timestep $t$, the RNN read the current input $x_t$ and updates its hidden state $\bm{h}$ by:
\begin{equation}
\bm{h}_t = f_\theta(x_t, \bm{h}_{t-1}),
\end{equation}
where $f$ is a deterministic non-linear transition function, and $\theta$ is the parameters of $f$. There are multiple ways to implement $f$, ranging from simple Vanilla RNN to long short-term memory. We will try different implementation of RNN and empirically compare their performance. 
\paragraph{Encodings Generation}
For segmentation task, we need to generate encodings for each team. Hence, we add another function (encoding layer) to map the output (hidden state) from RNN to an encoding. We can do this by implementing a single fully connected dense layer. 
\paragraph{Put it all together}
% \begin{table}
  
% \end{table}
Fig~\ref{fig:rnn} shows an overview of our final model for team segmentation task. It consists of three components - RNN, encoding layer and output layer. A high-level summary of the network is shown as Table~\ref{tbl:summary}.
Our input $\bm{X}$ is the shotlog sequence data, and $\bm{Y}$ is a one-hot vector representing which team does $\bm{X}$ belong to. 
After training the network on the shotlog data, we use the weights from the RNN and encoding layer to produce a set of encodings for each team - after feeding in a team's shotlog sequence, we would get a $N$-dimensional numeric encoding vector.
It's worthy mentioning that the goal of our model defined as Fig~\ref{fig:rnn} is not to train a classifier of teams given their shotlogs, but to obtain encodings for each team, and then cluster teams in the new encoding space.

\section{Preliminary Results}
\begin{figure}
\centering
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{baseline_clustering.png}
  \caption{Baseline Segmentation using MLP}
  \label{fig:baseline_clustering}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{LSTM_RNN_clustering.png}
  \caption{(LSTM) RNN Segmentation}
  \label{fig:rnn_clustering}
\end{minipage}
\end{figure}
\subsection{Baseline Segmentation Model vs. RNN Segmentation Model}
We first start with a simple baseline model. It follows the same structrue as Fig~\ref{fig:rnn}, but replace the RNN with a multilayer perceptron (MLP) network. In experiments, we set the number of layers of MLP as 3, each of which contains 1024 hidden neurons. 
MLP is unable to handle variable input sequence, hence we use padding to make variable input sequence data into fixed-length input. More specifically, for each input sequence $\bm{X} = \{(t_0,x_0,y_0), \dots, (t_n,x_n,y_n)\}$, let's define a matrix $M_{\bm{X}}$ of dimensions $2400 \times 2$. The length of a NCAA game is $40$ minutes $ = 2400$ seconds.\footnote{Here, we don't consider overtime of a game.} If there is an entry $(t_i, x_i, y_i)\in \bm{X}$, we set $M_{\bm{x}}[t_i][0] = x_i$ and $M_{\bm{x}}[t_i][1] = y_i$. After this transformation, the fixed-size matrix $M_{\bm{x}}$ equivalently represents the variable length play-by-play shotlog data in each game. 

Then, we implemented a LSTM RNN-based segmentation model, which directly digests the shotlog sequence data $\bm{X}$.

\subsection{Results}
After training, each team is represented by a encoding vector $\bm{v}_i \in \mathbb{R}^{m}$, where $m$ is the dimension of the encoding space. \footnote{In experiments, we set $m=32$ for both models.} 
We use standard Principal Component Analysis (PCA) to reduce encoding vectors' dimension from $m$ to $2$ for a better visualiation. Then we run a $K$-Means clustering procedure to produce the segmentation results. \footnote{We try multiple setting on value of $K$ to determine the best one.}
Results for baseline model and RNN model are shown as Fig~\ref{fig:baseline_clustering} and Fig~\ref{fig:rnn_clustering}, respectively.
Each point is a team, and different colors represent differen groups.
Segmentation from baseline model outputs a coarse clustering, where most of the teams belong to the same big cluster. 
MLP has limited ability to model sequence data (we have to transform sequence data into a big sparse matrix), and thus is incapable of learning a rich and informative features for each team, which in turn leads to an imperfect segmentation. On the other hand, RNN could capture the temporal dependence among sequential shotlogs and learn more subtle differences among teams given their play-by-play shotlogs. Compared to baseline model, RNN-based model produces fine-grained and more meaningful clusters.


\section{Future Steps}
Preliminary Results demonstrate the effectiveness of RNN on modeling sequential data, espectially on the task of NCAA team segmentation.
Here are few future steps needed toward the final report:
\begin{itemize}
  \item Try more complexed structure of RNN, i.e., Bidirectional RNN, stacked LSTM RNN and RNN with attention mechanism, to see whether the increase in network complexity could leads to better segmentation results.
  \item An evaluation methodology should be considered to compare the segmentation results, instead of just eyeballing. A feasible solution is to establish some ground truth of similarities among different teams from other available sources, i.e., team rankings in NCAA, win/loss ratio over the history, etc.
  \item Train and experiment RNN-based generative model for synthetic play generation.
\end{itemize} 

% \section{Submission of papers to NIPS 2018}

% NIPS requires electronic submissions.  The electronic submission site
% is
% \begin{center}
%   \url{https://cmt.research.microsoft.com/NIPS2018/}
% \end{center}

% Please read the instructions below carefully and follow them faithfully.

% \subsection{Style}

% Papers to be submitted to NIPS 2018 must be prepared according to the
% instructions presented here. Papers may only be up to eight pages
% long, including figures. Additional pages \emph{containing only
%   acknowledgments and/or cited references} are allowed. Papers that
% exceed eight pages of content (ignoring references) will not be
% reviewed, or in any other way considered for presentation at the
% conference.

% The margins in 2018 are the same as since 2007, which allow for
% $\sim$$15\%$ more words in the paper compared to earlier years.

% Authors are required to use the NIPS \LaTeX{} style files obtainable
% at the NIPS website as indicated below. Please make sure you use the
% current files and not previous versions. Tweaking the style files may
% be grounds for rejection.

% \subsection{Retrieval of style files}

% The style files for NIPS and other conference information are
% available on the World Wide Web at
% \begin{center}
%   \url{http://www.nips.cc/}
% \end{center}
% The file \verb+nips_2018.pdf+ contains these instructions and
% illustrates the various formatting requirements your NIPS paper must
% satisfy.

% The only supported style file for NIPS 2018 is \verb+nips_2018.sty+,
% rewritten for \LaTeXe{}.  \textbf{Previous style files for \LaTeX{}
%   2.09, Microsoft Word, and RTF are no longer supported!}

% The \LaTeX{} style file contains three optional arguments: \verb+final+,
% which creates a camera-ready copy, \verb+preprint+, which creates a
% preprint for submission to, e.g., arXiv, and \verb+nonatbib+, which will
% not load the \verb+natbib+ package for you in case of package clash.

% \paragraph{New preprint option for 2018}
% If you wish to post a preprint of your work online, e.g., on arXiv,
% using the NIPS style, please use the \verb+preprint+ option. This will
% create a nonanonymized version of your work with the text
% ``Preprint. Work in progress.''  in the footer. This version may be
% distributed as you see fit. Please \textbf{do not} use the
% \verb+final+ option, which should \textbf{only} be used for papers
% accepted to NIPS.

% At submission time, please omit the \verb+final+ and \verb+preprint+
% options. This will anonymize your submission and add line numbers to aid
% review. Please do \emph{not} refer to these line numbers in your paper
% as they will be removed during generation of camera-ready copies.

% The file \verb+nips_2018.tex+ may be used as a ``shell'' for writing
% your paper. All you have to do is replace the author, title, abstract,
% and text of the paper with your own.

% The formatting instructions contained in these style files are
% summarized in Sections \ref{gen_inst}, \ref{headings}, and
% \ref{others} below.

% \section{General formatting instructions}
% \label{gen_inst}

% The text must be confined within a rectangle 5.5~inches (33~picas)
% wide and 9~inches (54~picas) long. The left margin is 1.5~inch
% (9~picas).  Use 10~point type with a vertical spacing (leading) of
% 11~points.  Times New Roman is the preferred typeface throughout, and
% will be selected for you by default.  Paragraphs are separated by
% \nicefrac{1}{2}~line space (5.5 points), with no indentation.

% The paper title should be 17~point, initial caps/lower case, bold,
% centered between two horizontal rules. The top rule should be 4~points
% thick and the bottom rule should be 1~point thick. Allow
% \nicefrac{1}{4}~inch space above and below the title to rules. All
% pages should start at 1~inch (6~picas) from the top of the page.

% For the final version, authors' names are set in boldface, and each
% name is centered above the corresponding address. The lead author's
% name is to be listed first (left-most), and the co-authors' names (if
% different address) are set to follow. If there is only one co-author,
% list both author and co-author side by side.

% Please pay special attention to the instructions in Section \ref{others}
% regarding figures, tables, acknowledgments, and references.

% \section{Headings: first level}
% \label{headings}

% All headings should be lower case (except for first word and proper
% nouns), flush left, and bold.

% First-level headings should be in 12-point type.

% \subsection{Headings: second level}

% Second-level headings should be in 10-point type.

% \subsubsection{Headings: third level}

% Third-level headings should be in 10-point type.

% \paragraph{Paragraphs}

% There is also a \verb+\paragraph+ command available, which sets the
% heading in bold, flush left, and inline with the text, with the
% heading followed by 1\,em of space.

% \section{Citations, figures, tables, references}
% \label{others}

% These instructions apply to everyone.

% \subsection{Citations within the text}

% The \verb+natbib+ package will be loaded for you by default.
% Citations may be author/year or numeric, as long as you maintain
% internal consistency.  As to the format of the references themselves,
% any style is acceptable as long as it is used consistently.

% The documentation for \verb+natbib+ may be found at
% \begin{center}
%   \url{http://mirrors.ctan.org/macros/latex/contrib/natbib/natnotes.pdf}
% \end{center}
% Of note is the command \verb+\citet+, which produces citations
% appropriate for use in inline text.  For example,
% \begin{verbatim}
%    \citet{hasselmo} investigated\dots
% \end{verbatim}
% produces
% \begin{quote}
%   Hasselmo, et al.\ (1995) investigated\dots
% \end{quote}

% If you wish to load the \verb+natbib+ package with options, you may
% add the following before loading the \verb+nips_2018+ package:
% \begin{verbatim}
%    \PassOptionsToPackage{options}{natbib}
% \end{verbatim}

% If \verb+natbib+ clashes with another package you load, you can add
% the optional argument \verb+nonatbib+ when loading the style file:
% \begin{verbatim}
%    \usepackage[nonatbib]{nips_2018}
% \end{verbatim}

% As submission is double blind, refer to your own published work in the
% third person. That is, use ``In the previous work of Jones et
% al.\ [4],'' not ``In our previous work [4].'' If you cite your other
% papers that are not widely available (e.g., a journal paper under
% review), use anonymous author names in the citation, e.g., an author
% of the form ``A.\ Anonymous.''

% \subsection{Footnotes}

% Footnotes should be used sparingly.  If you do require a footnote,
% indicate footnotes with a number\footnote{Sample of the first
%   footnote.} in the text. Place the footnotes at the bottom of the
% page on which they appear.  Precede the footnote with a horizontal
% rule of 2~inches (12~picas).

% Note that footnotes are properly typeset \emph{after} punctuation
% marks.\footnote{As in this example.}

% \subsection{Figures}

% \begin{figure}
%   \centering
%   \fbox{\rule[-.5cm]{0cm}{4cm} \rule[-.5cm]{4cm}{0cm}}
%   \caption{Sample figure caption.}
% \end{figure}

% All artwork must be neat, clean, and legible. Lines should be dark
% enough for purposes of reproduction. The figure number and caption
% always appear after the figure. Place one line space before the figure
% caption and one line space after the figure. The figure caption should
% be lower case (except for first word and proper nouns); figures are
% numbered consecutively.

% You may use color figures.  However, it is best for the figure
% captions and the paper body to be legible if the paper is printed in
% either black/white or in color.

% \subsection{Tables}

% All tables must be centered, neat, clean and legible.  The table
% number and title always appear before the table.  See
% Table~\ref{sample-table}.

% Place one line space before the table title, one line space after the
% table title, and one line space after the table. The table title must
% be lower case (except for first word and proper nouns); tables are
% numbered consecutively.

% Note that publication-quality tables \emph{do not contain vertical
%   rules.} We strongly suggest the use of the \verb+booktabs+ package,
% which allows for typesetting high-quality, professional tables:
% \begin{center}
%   \url{https://www.ctan.org/pkg/booktabs}
% \end{center}
% This package was used to typeset Table~\ref{sample-table}.

% \begin{table}
%   \caption{Sample table title}
%   \label{sample-table}
%   \centering
%   \begin{tabular}{lll}
%     \toprule
%     \multicolumn{2}{c}{Part}                   \\
%     \cmidrule(r){1-2}
%     Name     & Description     & Size ($\mu$m) \\
%     \midrule
%     Dendrite & Input terminal  & $\sim$100     \\
%     Axon     & Output terminal & $\sim$10      \\
%     Soma     & Cell body       & up to $10^6$  \\
%     \bottomrule
%   \end{tabular}
% \end{table}

% \section{Final instructions}

% Do not change any aspects of the formatting parameters in the style
% files.  In particular, do not modify the width or length of the
% rectangle the text should fit into, and do not change font sizes
% (except perhaps in the \textbf{References} section; see below). Please
% note that pages should be numbered.

% \section{Preparing PDF files}

% Please prepare submission files with paper size ``US Letter,'' and
% not, for example, ``A4.''

% Fonts were the main cause of problems in the past years. Your PDF file
% must only contain Type 1 or Embedded TrueType fonts. Here are a few
% instructions to achieve this.

% \begin{itemize}

% \item You should directly generate PDF files using \verb+pdflatex+.

% \item You can check which fonts a PDF files uses.  In Acrobat Reader,
%   select the menu Files$>$Document Properties$>$Fonts and select Show
%   All Fonts. You can also use the program \verb+pdffonts+ which comes
%   with \verb+xpdf+ and is available out-of-the-box on most Linux
%   machines.

% \item The IEEE has recommendations for generating PDF files whose
%   fonts are also acceptable for NIPS. Please see
%   \url{http://www.emfield.org/icuwb2010/downloads/IEEE-PDF-SpecV32.pdf}

% \item \verb+xfig+ "patterned" shapes are implemented with bitmap
%   fonts.  Use "solid" shapes instead.

% \item The \verb+\bbold+ package almost always uses bitmap fonts.  You
%   should use the equivalent AMS Fonts:
% \begin{verbatim}
%    \usepackage{amsfonts}
% \end{verbatim}
% followed by, e.g., \verb+\mathbb{R}+, \verb+\mathbb{N}+, or
% \verb+\mathbb{C}+ for $\mathbb{R}$, $\mathbb{N}$ or $\mathbb{C}$.  You
% can also use the following workaround for reals, natural and complex:
% \begin{verbatim}
%    \newcommand{\RR}{I\!\!R} %real numbers
%    \newcommand{\Nat}{I\!\!N} %natural numbers
%    \newcommand{\CC}{I\!\!\!\!C} %complex numbers
% \end{verbatim}
% Note that \verb+amsfonts+ is automatically loaded by the
% \verb+amssymb+ package.

% \end{itemize}

% If your file contains type 3 fonts or non embedded TrueType fonts, we
% will ask you to fix it.

% \subsection{Margins in \LaTeX{}}

% Most of the margin problems come from figures positioned by hand using
% \verb+\special+ or other commands. We suggest using the command
% \verb+\includegraphics+ from the \verb+graphicx+ package. Always
% specify the figure width as a multiple of the line width as in the
% example below:
% \begin{verbatim}
%    \usepackage[pdftex]{graphicx} ...
%    \includegraphics[width=0.8\linewidth]{myfile.pdf}
% \end{verbatim}
% See Section 4.4 in the graphics bundle documentation
% (\url{http://mirrors.ctan.org/macros/latex/required/graphics/grfguide.pdf})

% A number of width problems arise when \LaTeX{} cannot properly
% hyphenate a line. Please give LaTeX hyphenation hints using the
% \verb+\-+ command when necessary.

\subsubsection*{Statement}

I adhered to the Duke Community Standard in the completion of this project.

\section*{References}

\small

[1] Shah, Rajiv, and Rob Romijnders. "Applying deep learning to basketball trajectories." arXiv preprint arXiv:1608.03793 (2016).

[2] Graves, Alex. "Generating sequences with recurrent neural networks." arXiv preprint arXiv:1308.0850 (2013).

[3] Chung, Junyoung, Kyle Kastner, Laurent Dinh, Kratarth Goel, Aaron C. Courville, and Yoshua Bengio. "A recurrent latent variable model for sequential data." In Advances in neural information processing systems, pp. 2980-2988. 2015.

[4] S. Hochreiter and J. Schmidhuber. Long short-term memory. Neural Computation, 9(8):1735–1780,
1997.



\end{document}
