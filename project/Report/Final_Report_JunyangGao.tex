\documentclass{article}

% if you need to pass options to natbib, use, e.g.:
% \PassOptionsToPackage{numbers, compress}{natbib}
% before loading nips_2018

% ready for submission
% \usepackage{nips_2018}

% to compile a preprint version, e.g., for submission to arXiv, add
% add the [preprint] option:
\usepackage[preprint]{nips_2018}

% to compile a camera-ready version, add the [final] option, e.g.:
% \usepackage[final]{nips_2018}

% to avoid loading the natbib package, add option nonatbib:
% \usepackage[nonatbib]{nips_2018}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{graphicx}
\usepackage{bm}
\usepackage{amsthm}
\usepackage{caption}
 
\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]


\title{Applying Sequence Model to Basketball Play-by-Play Data Analysis}

% The \author macro works with any number of authors. There are two
% commands used to separate the names and addresses of multiple
% authors: \And and \AND.
%
% Using \And between authors leaves it to LaTeX to determine where to
% break the lines. Using \AND forces a line break at that point. So,
% if LaTeX puts 3 of 4 authors names on the first line, and the last
% on the second line, try using \AND instead of \And before the third
% author name.

\author{
  Junyang Gao \\
  \texttt{jygao@cs.duke.edu} \\
  %% examples of more authors
  %% \And
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
  %% \AND
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
  %% \And
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
  %% \And
  %% Coauthor \\
  %% Affiliation \\
  %% Address \\
  %% \texttt{email} \\
}

\begin{document}
% \nipsfinalcopy is no longer used

\maketitle

\begin{abstract}
  In this project, we explore the play-by-play shotlog data in National Collegiate Athletic Association (NCAA) baseketball games by applying recurrent neural network (RNN). 
  Play-by-play data reveals the \emph{temporal} dependence of each event of the game, and RNN could learn and model the temporal dynamics from structured sequence data. 
  Our hope is to combine deep sequence model and play-by-play data to provide more insights into traditional sports data analytics.
  More specifically, we focus on two novel and interesting tasks: (1) Team Segmentation and (2) Synthetic Play Generation.
  The first task on team segmentation is to find groups of similar NCAA teams. We use a RNN to generate encodings (results of intermediate layers) for sequence play-by-play data of each team, and then apply clustering algorithm in the encoding space to identify groups. 
  We argue that RNN and encodings of a neural network provides be an effective and novel way for clsutering teams. 
  The segmentation results could provide team coaches, players and basketball fans a better understanding of the NCAA games and teams.
  The second task on synthetic play generation is a by-product of RNN. Given a trained RNN, it can be used as a generative model by using its internal hidden states. Though it's generally hard to evaluate the synthetic play outcomes, the task still provides interesting and (possibly) insightful results for both professionals and fans.



  
\end{abstract}

\section{Introduction}
\label{sec:intro}
There are emerging trends for sports data analytics of using player and ball tracking data — Since 2013, NBA has applied an advanced tracking system to produce real-time player and ball positional data for every single game; In 2015, Duke led the College Basketball’s data revolution, being the first NCAA team to embrace the cutting-edge tracking system for generating play-by-play motion and tracking data. This sea of information provides us with unlimited probabilities for more insights of the game.
Despite the fact that we have easy access to the large amount of high quality tracking data, how to effectively analyze this new form of the data has been largely ignored by prior research. In this project, we aim to go beyond the traditional box score or statistics, combining deep learning techniques (specifically, Sequence Models) and detailed play-by-play data logs to produce novel analytics, including clustering and synthetic play generation, that traditional data mining algorithms or machine learning models can’t handle.

\section{Data Description}
\label{sec:data}
There are many types of play-by-play data, i.e., ball movement and player movement. 
In this project, we focus on the team's shot logs, which mainly records the shooting location (XY\--coordinates), shooting time\--stamp and other related labels of the shot, including shot types (three\-pointer, jump shot, layup), outcomes (miss or made), etc.
Given a team and a game, we use a sequence of tuples organized by time $X_i = \{(t_1,x_1,y_1, l_1),(t_2,x_2,y_2, l_2), \cdots, (t_n, x_n, y_n, l_n)\}$ to represent its shots logs, where $t$ is the shooting time\--stamp in seconds, $xy$ is the shooting location in the basketball court and $l$ is the label associated with the shot. More specifically, $x, y$ represent the location of the shot in number of inches from the "left" baseline (max: 1128) and from the ``top'' sideline (max: 600), respectively. 

\subsection{Data Source}
\emph{Google BigQuery} hosts a historical play-by-play shotlog dataset \footnote{\url{https://bigquery.cloud.google.com/table/bigquery-public-data:ncaa_basketball.mbb_pbp_sr?pli=1}} containing NCAA tournament games from 2013 to 2017. 
We queried the entire dataset and downloaded the query result as our data. It can be find at our gitlab repository. \footnote{\url{https://gitlab.oit.duke.edu/jg308/IntroDeepLearning/blob/master/project/ncaa-shotlog.csv}}.  
The dataset contains 19,494 shotlogs from 231 distinct NCAA teams, where each shotlog is a shot sequence with variable length (i.e, how many shots made by the team in a game) as $X_i$ ordered by time. 

\subsection{Data Preprocessing}
\paragraph{Team Pruning}
Games are not evenly distributed over 231 teams - the max number of games from a team is 478, while the min number is just 1. 
We need enough games to sufficiently learn and model the style of each team. Hence, we prune the teams with games recorded less than 100. 
After the prunning, we have 15,464 game shotlogs from 85 distinct NCAA teams, averaging 182 games for each team.

\paragraph{Shot Position Transformation}
\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{shotlogs_visual}
  \caption{Shotlog Visualization\label{fig:shotlog_visual}}
\end{figure}
A NCAA game consists of two 20-minutes halves, and teams switch baskets to goal at halftime. If we look at the raw shotlog data with their absolute shooting coordinates, there is clearly a position shift from one section of the court to another, as shown in fig~\ref{fig:shotlog_visual}(a). In order to get rid of the suddent changes in shooting coordinates, we limit the shooting position to the left side of the court, and apply the following the transformation of positions on the right side of the court:
\begin{equation}
(x',y') = (1128 - x, 600 - y),
\end{equation} 
shown as fig~\ref{fig:shotlog_visual}(b).

\section{Problem Statement}
In this project, we focus on tackling the following two novel and interesting tasks in sports data analytics: (1) Team Segmentation, and (2) Synthetic Play Generation.

\paragraph{Team Segmentation}
Analytically, team segmentations involve clustering all NCAA teams to find groups of similar teams. 
Understanding team segments could provide deeper insights into the game - team coaches might design suitable strategies to teams from different segments (instead of for each individual game!) for the win, and basketball fans may find it an interesting topic to discuss.
However, it's not clear how to define ``similar'' in the context of basketball team - by demographic? by win/loss ratio? 
There is not a gold standard for similarity. On the other hand, traditionally, the data that goes into the clustering is often limited by the clsutering algorithms themselves - most require some kind of tabular data structure, e.g., distance matrix, and common techniques like $k$-Means require strictly numeric input.
Intuitively, teams should be segmented or clustered by the games they played. With play-by-play shotlog data in hand, we are capable of understanding the style or tendencies of shots made by a team, and then group team based on these features.
Given the popularity of deep learning models and techniques, i.e., RNN, to tackle sequence-related learning tasks, it's natural to use a RNN to learn the hidden features (encodings) that represents a team.
\paragraph{Synthetic Play Generation}
After learning the shotlog sequence data for each team using RNN, the internal hidden states encode the temporal dependences among sequence data. Given an initial input, the network can also be used as a generative model to automatically generate synthetic shots that follows the similar trends of the training data. 
Although we realize that it's empirically hard to evaluate shot generations, the synthetic generative outcomes still provides interesting and (possibly) insightful results for both professionals and fans.

\section{Core Methods}

\subsection{RNN-based Team Segmentation}
\begin{figure}
\centering
\begin{minipage}{0.45\textwidth}
\includegraphics[width=\linewidth]{RNN_Encoding.png}
\caption{Network Architecture \label{fig:rnn}}
\end{minipage}
\begin{minipage}{0.45\textwidth}
\captionsetup{type=table} %% tell latex to change to table
\caption{Network Summary}
  \label{tbl:summary}
  \centering
  \small
  \begin{tabular}{lll}
    \toprule
    Layer& Input Dimension & Output Dimension \\
    \toprule
    RNN & Variable & N (tunning hyperparameters) \\
    Encoding & N & N (used for encoding) \\
    Output & N & n (distinct number of teams) \\
    \bottomrule
  \end{tabular}
\end{minipage}
\end{figure}
In this section, we present our approach to solve the Team Segmentation problem.
\paragraph{Sequence Modeling with RNN}
An RNN can take as input a variable-length sequence $x = (x_1, x_2, \dots, x_T)$ by recursively processing each symbol while maintaining its internal hidden state $\bm{h}$. At each timestep $t$, the RNN read the current input $x_t$ and updates its hidden state $\bm{h}$ by:
\begin{equation}
\bm{h}_t = f_\theta(x_t, \bm{h}_{t-1}),
\end{equation}
where $f$ is a deterministic non-linear transition function, and $\theta$ is the parameters of $f$. There are multiple ways to implement $f$, ranging from simple Vanilla RNN to long short-term memory. We will try different implementation of RNN and empirically compare their performance. 
\paragraph{Encodings Generation}
For segmentation task, we need to generate encodings for each team. Hence, we add another function (encoding layer) to map the output (hidden state) from RNN to an encoding. We can do this by implementing a single fully connected dense layer. 
\paragraph{Put it all together}
% \begin{table}
  
% \end{table}
Fig~\ref{fig:rnn} shows an overview of our final model for team segmentation task. It consists of three components - RNN, encoding layer and output layer. A high-level summary of the network is shown as Table~\ref{tbl:summary}.
Our input $\bm{X}$ is the shotlog sequence data, and $\bm{Y}$ is a one-hot vector representing which team does $\bm{X}$ belong to. 
During model training, we minimize the cross-entropy loss at the final output layer.
After training the network on the shotlog data, we use the weights from the RNN and encoding layer to produce a set of encodings for each team. Each encoding is represented as a $N$-dimensional numeric encoding vector.
It's worthy mentioning that the goal of our model defined as Fig~\ref{fig:rnn} is not to train a classifier of teams given their shotlogs, but to obtain encodings for each team, and then cluster teams in the new encoding space.

\subsection{RNN-based Synthetic Play Generation}
\begin{figure}
\centering
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{shot_train.png}
  \caption{Model Learning}
  \label{fig:rnn-training}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=\linewidth]{shot_gen.png}
  \caption{Play Generation}
  \label{fig:rnn-generation}
\end{minipage}
\end{figure}

In this section, we use RNN to learn a set of sequence shot logs, and then use the trained model to generate synthetic shots that has similar style as our training data.

\paragraph{Model Training}
Directly predicting the exact 2D shot location might result in overfitting. 
To overcome this challenge, we discretize the basketball half-court into grid. 
Each cell in the gird is a 5 $\times$ 5 square. Then, every shot is encoded by a one-hot vector indicating the cell index in the grid. During model traning, the prediction is made on the cell index.
The learning is unsupervised, shown as Fig~\ref{fig:rnn-training}. At each time step $t$, the model predicts the next shot location $\hat{X_t}$. The loss function is the summation of cross-entropy error (between $\hat{X_t}$ and $X_{t+1}$) among all timestamps.

\paragraph{Play Generation}
After training, the model could generate synthetic shot logs by itself! 
As shown in Fig~\ref{fig:rnn-generation},
given an initial start shot location (or cell index in the gird), a sequence of shot could be produced by iteratively feeding the shot prediction, i.e., $\hat{X_t}$, from a RNN cell as input to the next RNN cell.
The sample process is to generate the exact shot location by uniformly sample a point in the specific grid cell.



\section{Experiments}

\subsection{Baseline Segmentation Model vs. RNN Segmentation Model}
We first start with a simple baseline model. It follows the same structrue as Fig~\ref{fig:rnn}, but replace the RNN with a multilayer perceptron (MLP) network. In experiments, we set the number of layers of MLP as 3, each of which contains 1024 hidden neurons. 
MLP is unable to handle variable input sequence, hence we use padding to make variable input sequence data into fixed-length input. More specifically, for each input sequence $\bm{X} = \{(t_0,x_0,y_0), \dots, (t_n,x_n,y_n)\}$, let's define a matrix $M_{\bm{X}}$ of dimensions $2400 \times 2$. The length of a NCAA game is $40$ minutes $ = 2400$ seconds.\footnote{Here, we don't consider overtime of a game.} If there is an entry $(t_i, x_i, y_i)\in \bm{X}$, we set $M_{\bm{x}}[t_i][0] = x_i$ and $M_{\bm{x}}[t_i][1] = y_i$. After this transformation, the fixed-size matrix $M_{\bm{x}}$ equivalently represents the variable length play-by-play shotlog data in each game. 
Then, we implemented a LSTM RNN-based segmentation model, which directly digests the shotlog sequence data $\bm{X}$.

After training, each team is represented by a encoding vector $\bm{v}_i \in \mathbb{R}^{m}$, where $m$ is the dimension of the encoding space. \footnote{In experiments, we set $m=32$ for both models.} 
We use standard Principal Component Analysis (PCA) to reduce encoding vectors' dimension from $m$ to $2$ for a better visualiation. Then we run a $K$-Means clustering procedure to produce the segmentation results. \footnote{We try multiple setting on value of $K$ to determine the best one.}
Results for baseline model and RNN model are shown as Fig~\ref{fig:baseline_clustering} and Fig~\ref{fig:rnn_clustering}, respectively.
Each point is a team, and different colors represent differen groups.
Segmentation from baseline model outputs a coarse clustering, where most of the teams belong to the same big cluster. 
MLP has limited ability to model sequence data (we have to transform sequence data into a big sparse matrix), and thus is incapable of learning a rich and informative features for each team, which in turn leads to an imperfect segmentation. On the other hand, RNN could capture the temporal dependence among sequential shotlogs and learn more subtle differences among teams given their play-by-play shotlogs. Compared to baseline model, RNN-based model produces fine-grained and more meaningful clusters.

\paragraph{Result Validation}

There are multiple criteria of evaluating a basketball team such as offensive ranking, defensive ranking, assist/turnover ratio, etc. Here, we choose offensive ranking as the metric to justify the quality of our segmentation results. 
The full offensive rankings of all NCAA teams are available at NCAA official website \footnote{\url{https://www.ncaa.com/stats/basketball-men/d1/current/team/145}}. 
The RNN-based segmentation method identifies 7 distinct clusters with some significant, and valuable differences between them. We summarized the average offensive rankings within each clusters as in Table~\ref{tbl:validation}. Results shown that offensive ability of the team seems a strong driver in defining segments, which is not captured by baseline method.
However, there are clearly other hidden factors influencing the segments. 
Our RNN-based segmentation results could be also used as a guideline for data analysts or sport experts to mine more interesting connections and relationships among teams.

\begin{center}
\captionsetup{type=table} %% tell latex to change to table
\caption{Cluster Analysis}
  \label{tbl:validation}
\begin{tabular}{ c | c | c | c}
\hline
 Segment & Percentage of teams & Avg. Offensive Rankings & Random Representative Team\\\hline
 1 & 0.141 & 190 & California Golden Bears \\ 
 2 & 0.16 & 150 & Indiana Hoosiers \\ 
 3 & 0.141 & 94 & Georgetown Hoyas  \\ 
 4 & 0.335 & 157 &  Wisconsin Badgers\\ 
 5 & 0.058 & 142 &  Marquette Golden Eagles\\ 
 6 & 0.08 & 47 & UCLA Bruins \\
 7 & 0.094 & 30 & Kansas Jayhawks\\   
\end{tabular}
\end{center}


\begin{figure}
\centering
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{baseline_clustering.png}
  \caption{Baseline Segmentation using MLP}
  \label{fig:baseline_clustering}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=0.8\linewidth]{LSTM_RNN_clustering.png}
  \caption{(LSTM) RNN Segmentation}
  \label{fig:rnn_clustering}
\end{minipage}
\end{figure}

\subsection{Synthetic Play Generation}

In this section, we demonstrate the generative ability of RNN model and compare the results with real shot logs. 
We use 8 games of Duke Blue Devils as training data, and shot visualizations is given in Fig~\ref{fig:real-shots}. As a comparison, RNN model generates 8 synthetic game plays using 8 randomly choosen initial shot locations. Synthetic shot visualizations is given in Fig~\ref{fig:fake-shots}. 
For simplicity, for both traning data and synthetic sequence, we use a fixed length of 100.
From Fig~\ref{fig:real-shots} and Fig~\ref{fig:fake-shots},
we can clearly see that our synthetic plays learn Duke's offensive preference of shooting 3-pointers and attacking the basket.


\begin{figure}
\centering
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{real_play.png}
  \caption{Real shot logs by Duke Blue Devils}
  \label{fig:real-shots}
\end{minipage}%
\begin{minipage}{.5\textwidth}
  \centering
  \includegraphics[width=0.9\linewidth]{fake_play.png}
  \caption{Synthetic shot logs generated by RNN}
  \label{fig:fake-shots}
\end{minipage}
\end{figure}

\section{Discussions and Future Work}

Sequence data is increasingly being captured in sports games. Accordingly, novel techniques should also be developed to explore it.
In this project, we apply RNN-based sequence model to learn from sequence data and receive some promising results in the task of team segmentation and synthetic play generation.
Here, we summarize some main takeaways:

\begin{itemize}
  \item Compared with traditional machine learning techniques or basic neuron networks, sequence model has the flexibility of handling sequence data in either fixed or variable length.
  \item Sequence data expresses the hidden temporal dependence among data points. For example, if we simply visualize the shots in a 2D space, we ignore the information on time dimension. In some cases, maybe two teams have similar shot distributions, but the shots may not be made in the same order. 
  \item Sequence model digests sequential data time step by time step, which perfectly capture the hidden connections of data points over time domain. 
  Other models like MLP mostly treat the temporal information as an independent feature of input. 
  This major difference is the main reason why RNN could learn a richer and more information encodings for each team, and further lead to a better segmentation on teams.
  \item The iterative structure of RNN natrually makes it a generative model for sequential data.
\end{itemize}

In this project, we take a few initial steps of applying RNN-based sequence model to explore sequential data analytics in basketball domain.
Going forward, there are still many interesting problems. 
To name a few, 
(1) similar to team segmentation task, how to classify player is also an interesting problem and has more meanful interpretations; 
(2) game outcome prediction. This is a more challenging task, and we have seen many previous work using various techniques to tackle this problem. 
Currently, it's not clear whether sequence model has the advantage over existing solutions. On the other hand, what input signals should be used and how to form sequence data by time dimension are still understudied. 

\section{Conclusion}
We successfully apply RNN-based deep learning models to develop novel analytics - team segmentation and synthetic play generations - from basketball play-by-play shotlogs. 
RNN could effectively generating encodings for basketball team by analyzing their sequential shotlogs. 
We further demonstrate that clustering the encodings (intermediate results from hidden layer) of a RNN is a better way to finding similarities among teams.
On the other hand, RNN could also be used as a generative model. Our preliminary results demonstate that a trained RNN is capable of  generating synthetic shot sequences that has the similar offensive preference as the training data.

% Different from traditional data mining algorithms or machine learning models, sequence models can: (1) capture and learn more informative features by exploring temporal dependence of sequence data, which could be further used for other analytical tasks, like data clustering or segmentation; (2) produce novel and interesting results such as generating a sequence of synthetic shots with specific style of a team.


% \section{Future Steps}
% Preliminary Results demonstrate the effectiveness of RNN on modeling sequential data, espectially on the task of NCAA team segmentation.
% Here are few future steps needed toward the final report:
% \begin{itemize}
%   \item Try more complexed structure of RNN, i.e., Bidirectional RNN, stacked LSTM RNN and RNN with attention mechanism, to see whether the increase in network complexity could leads to better segmentation results.
%   \item An evaluation methodology should be considered to compare the segmentation results, instead of just eyeballing. A feasible solution is to establish some ground truth of similarities among different teams from other available sources, i.e., team rankings in NCAA, win/loss ratio over the history, etc.
%   \item Train and experiment RNN-based generative model for synthetic play generation.
% \end{itemize} 


\subsubsection*{Statement}

I adhered to the Duke Community Standard in the completion of this project.

\section*{References}

\small

[1] Shah, Rajiv, and Rob Romijnders. "Applying deep learning to basketball trajectories." arXiv preprint arXiv:1608.03793 (2016).

[2] Graves, Alex. "Generating sequences with recurrent neural networks." arXiv preprint arXiv:1308.0850 (2013).

[3] Chung, Junyoung, Kyle Kastner, Laurent Dinh, Kratarth Goel, Aaron C. Courville, and Yoshua Bengio. "A recurrent latent variable model for sequential data." In Advances in neural information processing systems, pp. 2980-2988. 2015.

[4] S. Hochreiter and J. Schmidhuber. Long short-term memory. Neural Computation, 9(8):1735–1780,
1997.



\end{document}
